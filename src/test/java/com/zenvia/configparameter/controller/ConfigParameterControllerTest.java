package com.zenvia.configparameter.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.zenvia.configparameter.model.entity.ConfigParameter;
import com.zenvia.configparameter.service.ConfigParameterService;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(ConfigParameterController.class)
class ConfigParameterControllerTest {

  @Autowired
  private MockMvc mockMvc;
  @MockBean
  private ConfigParameterService configParameterService;

  @Test
  void shouldFindById() throws Exception {
    when(configParameterService.findById(1))
        .thenReturn(
            Optional.of(new ConfigParameter(1,"config", "desc", 2, "value")));

    mockMvc.perform(get("/configurations/1"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id", is(1)))
        .andExpect(jsonPath("$.confMnemonic", is("config")))
        .andExpect(jsonPath("$.confDescription", is("desc")))
        .andExpect(jsonPath("$.confType", is(2)))
        .andExpect(jsonPath("$.confValue", is("value")));
  }

  @Test
  void shouldReturnNotFoundWhenConfigurationNotFoundById() throws Exception {
    when(configParameterService.findById(1))
        .thenReturn(
            Optional.empty());

    mockMvc.perform(get("/configurations/1"))
        .andExpect(status().is(404));

  }
}