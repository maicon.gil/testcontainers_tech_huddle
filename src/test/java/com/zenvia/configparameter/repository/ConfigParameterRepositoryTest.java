package com.zenvia.configparameter.repository;

import static org.assertj.core.api.Assertions.assertThat;

import com.zenvia.configparameter.model.entity.ConfigParameter;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
class ConfigParameterRepositoryTest {

  @Autowired
  private ConfigParameterRepository configParameterRepository;

  @Test
  void whenFindByConfMnemonicContainingTextNotFoundAnyRecordShouldReturnEmptyList() {
    configParameterRepository.save(
        new ConfigParameter("conf","desc", 2, "value"));

    List<ConfigParameter> actual = configParameterRepository.findByConfMnemonicContaining("hello");

    assertThat(actual).isEmpty();
  }

  @Test
  void shouldFindByConfMnemonicContainingText() {
    ConfigParameter expected = configParameterRepository.save(
        new ConfigParameter("conf hello", "desc  desc", 2, "value"));

    List<ConfigParameter> actual = configParameterRepository.findByConfMnemonicContaining("hello");

    assertThat(actual).containsExactly(expected);
  }
}