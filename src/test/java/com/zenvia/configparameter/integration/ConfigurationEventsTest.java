package com.zenvia.configparameter.integration;

import static java.util.Collections.singleton;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zenvia.configparameter.model.dto.ConfigParameterDto;
import com.zenvia.configparameter.model.entity.ConfigParameter;
import com.zenvia.configparameter.repository.ConfigParameterRepository;
import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

@SpringBootTest
@AutoConfigureMockMvc
@Testcontainers
public class ConfigurationEventsTest {

  @Container
  static
  GenericContainer<?> database = new GenericContainer<>(
      DockerImageName.parse("nexus-pull.zenvia.com/zenvia-sms/postgres-sms:latest"))
      .withExposedPorts(5432);

  @Container
  static
  KafkaContainer kafka = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:6.2.1"));

  @DynamicPropertySource
  public static void overrideProps(DynamicPropertyRegistry registry){
    registry.add("spring.kafka.bootstrap-servers", () -> kafka.getBootstrapServers());
    registry.add("spring.datasource.url", () ->
        String.format("jdbc:postgresql://%s:%s/humangw", database.getHost(), database.getMappedPort(5432)));
  }

   @Autowired
   private MockMvc mockMvc;

   @Autowired
   private ObjectMapper objectMapper;

   @Autowired
   private ConfigParameterRepository configParameterRepository;

   private Consumer<String, String> kafkaConsumer;


  @BeforeEach
  void setUp() {
    Map<String, Object> configs = new HashMap<>(KafkaTestUtils.consumerProps(kafka.getBootstrapServers(), "test", "true" ));
    configs.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
    kafkaConsumer = new DefaultKafkaConsumerFactory<>(configs, new StringDeserializer(), new StringDeserializer()).createConsumer();
    kafkaConsumer.subscribe(singleton("CONFIGURATION_EVENTS"));
  }

  @Test
  void whenConfigurationSavedShouldSendEvent() throws Exception {

    var configParameterDto=
        new ConfigParameterDto(null, "test.config", "desc", 0, "value");

    mockMvc.perform(post("/configurations")
            .content(objectMapper.writeValueAsString(configParameterDto))
            .contentType("application/json"));

    ConsumerRecord<String, String> singleRecord = KafkaTestUtils.getSingleRecord(kafkaConsumer, "CONFIGURATION_EVENTS");
    assertThat(singleRecord).isNotNull();
    assertThat(singleRecord.value()).isEqualTo("Configuration test.config was SAVED");

    kafkaConsumer.close();
  }

  @Test
  void whenConfigurationDeletedShouldSendEvent() throws Exception {

    ConfigParameter configParameter = configParameterRepository.save(
        new ConfigParameter("test.config.delete", "desc", 0, "value"));

    mockMvc.perform(delete("/configurations/{id}", configParameter.getId()));

    ConsumerRecord<String, String> singleRecord = KafkaTestUtils.getSingleRecord(kafkaConsumer, "CONFIGURATION_EVENTS", 5000);
    assertThat(singleRecord).isNotNull();
    assertThat(singleRecord.value()).isEqualTo(String.format("Configuration with id %s was DELETED", configParameter.getId()));

    kafkaConsumer.close();
  }
}
