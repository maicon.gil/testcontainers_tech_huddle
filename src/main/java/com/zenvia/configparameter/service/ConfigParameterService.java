package com.zenvia.configparameter.service;

import com.zenvia.configparameter.model.entity.ConfigParameter;
import com.zenvia.configparameter.repository.ConfigParameterRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ConfigParameterService {

  private static final String TOPIC = "CONFIGURATION_EVENTS";
  private final KafkaTemplate<String, String> eventProducer;

  private final ConfigParameterRepository configParameterRepository;

  public ConfigParameterService(KafkaTemplate<String, String> eventProducer,
      ConfigParameterRepository configParameterRepository) {
    this.eventProducer = eventProducer;
    this.configParameterRepository = configParameterRepository;
  }

  public Optional<ConfigParameter> findById(Integer id){
    return configParameterRepository.findById(id);
  }

  public List<ConfigParameter> findAll(){
    return configParameterRepository.findAll();
  }

  public ConfigParameter save(ConfigParameter configParameter){
    ConfigParameter savedConfigParameter = configParameterRepository.save(configParameter);
    eventProducer.send(TOPIC, String.format("Configuration %s was SAVED", savedConfigParameter.getConfMnemonic()));
    return savedConfigParameter;
  }

  public void deleteById(Integer id){
    configParameterRepository.deleteById(id);
    eventProducer.send(TOPIC, String.format("Configuration with id %s was DELETED", id));
  }
}
