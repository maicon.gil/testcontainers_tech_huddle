package com.zenvia.configparameter.repository;

import com.zenvia.configparameter.model.entity.ConfigParameter;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConfigParameterRepository extends JpaRepository<ConfigParameter, Integer> {

  List<ConfigParameter> findByConfMnemonicContaining(String text);

}