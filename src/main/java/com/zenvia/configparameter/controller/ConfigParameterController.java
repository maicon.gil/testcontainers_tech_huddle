package com.zenvia.configparameter.controller;

import com.zenvia.configparameter.model.dto.ConfigParameterDto;
import com.zenvia.configparameter.model.mapper.ConfigParameterMapper;
import com.zenvia.configparameter.service.ConfigParameterService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/configurations")
public class ConfigParameterController {

  private final ConfigParameterService configParameterService;

  public ConfigParameterController(ConfigParameterService configParameterService) {
    this.configParameterService = configParameterService;
  }

  @GetMapping("/{id}")
  public ResponseEntity<ConfigParameterDto> findById(@PathVariable Integer id){
    return configParameterService.findById(id)
        .map(ConfigParameterMapper::toDto)
        .map(ResponseEntity::ok)
        .orElse(ResponseEntity.notFound().build());
  }
  @GetMapping
  public List<ConfigParameterDto> findAll(){
    return configParameterService.findAll().stream().map(ConfigParameterMapper::toDto).collect(
        Collectors.toList());
  }

  @PostMapping
  public ConfigParameterDto create(@RequestBody ConfigParameterDto configParameterDto){
    return ConfigParameterMapper.toDto(
        configParameterService.save(ConfigParameterMapper.toEntity(configParameterDto))
    );
  }

  @PutMapping("/{id}")
  public ResponseEntity<ConfigParameterDto> updateById(@PathVariable Integer id, @RequestBody ConfigParameterDto configParameterDto){
    return configParameterService.findById(id)
        .map(found -> {
          found.setConfMnemonic(configParameterDto.confMnemonic());
          found.setConfType(configParameterDto.confType());
          found.setConfDescription(configParameterDto.confDescription());
          found.setConfValue(configParameterDto.confValue());
          return found;
        })
        .map(configParameterService::save)
        .map(ConfigParameterMapper::toDto)
        .map(ResponseEntity::ok)
        .orElse(ResponseEntity.notFound().build());
  }

  @DeleteMapping("/{id}")
  public ResponseEntity deleteById(@PathVariable Integer id){
    configParameterService.deleteById(id);
    return ResponseEntity.noContent().build();
  }

}
