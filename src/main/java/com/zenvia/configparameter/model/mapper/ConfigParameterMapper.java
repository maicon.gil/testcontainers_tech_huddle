package com.zenvia.configparameter.model.mapper;

import com.zenvia.configparameter.model.dto.ConfigParameterDto;
import com.zenvia.configparameter.model.entity.ConfigParameter;


public class ConfigParameterMapper {

  public static ConfigParameterDto toDto(ConfigParameter configParameter){
    return new ConfigParameterDto(configParameter.getId(),
        configParameter.getConfMnemonic(),
        configParameter.getConfDescription(),
        configParameter.getConfType(),
        configParameter.getConfValue());
  }

  public static ConfigParameter toEntity(ConfigParameterDto configParameterDto){
    return new ConfigParameter(
        configParameterDto.confMnemonic(),
        configParameterDto.confDescription(),
        configParameterDto.confType(),
        configParameterDto.confValue());
  }

}
