package com.zenvia.configparameter.model.entity;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "config_parameter")
public class ConfigParameter {

  public ConfigParameter() {
  }

  public ConfigParameter(String confMnemonic, String confDescription, Integer confType,
      String confValue) {
    this.confMnemonic = confMnemonic;
    this.confDescription = confDescription;
    this.confType = confType;
    this.confValue = confValue;
  }

  public ConfigParameter(Integer id, String confMnemonic, String confDescription, Integer confType,
      String confValue) {
    this.id = id;
    this.confMnemonic = confMnemonic;
    this.confDescription = confDescription;
    this.confType = confType;
    this.confValue = confValue;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "config_parameter_conf_id_seq")
  @SequenceGenerator(
      name = "config_parameter_conf_id_seq",
      sequenceName = "config_parameter_conf_id_seq",
      allocationSize = 1)
  @Column(name = "conf_id", nullable = false)
  private Integer id;

  @Column(name = "conf_mnemonic", nullable = false, length = 100)
  private String confMnemonic;

  @Column(name = "conf_description", length = 150)
  private String confDescription;

  @Column(name = "conf_type", nullable = false)
  private Integer confType;

  @Column(name = "conf_value")
  private String confValue;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getConfMnemonic() {
    return confMnemonic;
  }

  public void setConfMnemonic(String confMnemonic) {
    this.confMnemonic = confMnemonic;
  }

  public String getConfDescription() {
    return confDescription;
  }

  public void setConfDescription(String confDescription) {
    this.confDescription = confDescription;
  }

  public Integer getConfType() {
    return confType;
  }

  public void setConfType(Integer confType) {
    this.confType = confType;
  }

  public String getConfValue() {
    return confValue;
  }

  public void setConfValue(String confValue) {
    this.confValue = confValue;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ConfigParameter that = (ConfigParameter) o;
    return id.equals(that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}