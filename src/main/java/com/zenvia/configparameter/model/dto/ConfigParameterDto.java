package com.zenvia.configparameter.model.dto;

public record ConfigParameterDto(Integer id, String confMnemonic, String confDescription, Integer confType, String confValue) {}
